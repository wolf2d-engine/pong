cmake_minimum_required(VERSION 3.24)
project(Wolf2DPong)

add_subdirectory(engine)

add_executable(Wolf2DPong
    src/config.cpp
    src/paddle.cpp
    src/scene.cpp
)

target_include_directories(Wolf2DPong
    PRIVATE inc
)

target_link_libraries(Wolf2DPong
    Wolf2D
)

wolf2d_pack_dir(Wolf2DPong data.wpak rsrc)
