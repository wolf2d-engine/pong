#include "scene.h"
#include "SDL_keycode.h"
#include "engine/engine.h"
#include "engine/input/handler.h"
#include "engine/logger.h"
#include "engine/scene.h"
#include "engine/sprite.h"
#include "engine/ui/sprite.h"
#include "globals.h"
#include "paddle.h"
#include <cstdlib>
#include <ctime>
#include <math.h>

#ifdef ENABLE_IMGUI
#include "imgui.h"
#endif

float moveSpeed = 64;

float dirX = 1;
float dirY = 1;

bool end = false;
bool isUserWin = false;

bool isMoving = false;
bool isMovingUp = false;

Wolf2D::Input::Handler exitHandler{"exit", [](Wolf2D::Input::Action action) {
                                     Wolf2D::Engine::getEngine()->shutdown();
                                   }};

PongScene::PongScene(Wolf2D::Engine &engine)
    : Wolf2D::Scene(engine), m_engine(engine) {
  // place all the thingies in the scene

  // ball is just a sprite lol
  int centerX = (engine.camera().w / 2) - 2; // 4 / 2 = 2
  int centerY = (engine.camera().h / 2) - 2; // 4 / 2 = 2
  ball = new Wolf2D::Sprite(engine,
                            *engine.resourceManager()->getResource("white.png"),
                            // position
                            centerX, centerY, 4, 4, 1,
                            // sheet data (shouldve just used Texture...)
                            4, 4, 0, {{"default", {0}}});

  srand(time(0));

  gameObjects.push_back(ball);

  // winner text
  centerX = (engine.uiCamera().w / 2) - 100; // 200 / 2 = 100
  centerY = (engine.uiCamera().h / 2) - 50;  // 100 / 2 = 50

  winText = new Wolf2D::UI::Sprite(
      engine, *engine.resourceManager()->getResource("winner.png"),
      // position
      centerX, centerY, 200, 100, 1,
      // sheet data
      200, 300, 0, {{"default", {0}}, {"userWin", {1}}, {"pcWin", {2}}});

  gameObjects.push_back(winText);

  zHandler = {"reset", [this](Wolf2D::Input::Action action) {
                if (action == Wolf2D::Input::Action::KEY_UP)
                  this->reset();
              }};

  engine.inputDispatcher()->addHandler(&zHandler);
  engine.inputDispatcher()->setBinding(SDLK_z, "reset");

  engine.inputDispatcher()->addHandler(&exitHandler);
  engine.inputDispatcher()->setBinding(SDLK_ESCAPE, "exit");

  paddleUpHandler = {"paddleUp", [this](Wolf2D::Input::Action action) {
                       isMovingUp = true;
                       isMoving = action == Wolf2D::Input::Action::KEY_DOWN;
                     }};

  paddleDownHandler = {"paddleDown", [this](Wolf2D::Input::Action action) {
                         isMovingUp = false;
                         isMoving = action == Wolf2D::Input::Action::KEY_DOWN;
                       }};

  engine.inputDispatcher()->addHandler(&paddleUpHandler);
  engine.inputDispatcher()->addHandler(&paddleDownHandler);
  engine.inputDispatcher()->setBinding(SDLK_w, "paddleUp");
  engine.inputDispatcher()->setBinding(SDLK_s, "paddleDown");

  userPaddle = new Paddle(m_engine, 10);
  pcPaddle = new Paddle(m_engine, engine.camera().w - 20);

  gameObjects.push_back(userPaddle);
  gameObjects.push_back(pcPaddle);

  reset();
}

void PongScene::reset() {
  // randomise dirX and dirY
  dirX = (rand() % 2) ? -1 : 1;
  dirY = (rand() % 2) ? -1 : 1;

  // center ball
  int centerX = (engine.camera().w / 2) - 2; // 4 / 2 = 2
  int centerY = (engine.camera().h / 2) - 2; // 4 / 2 = 2
  ball->position.x = centerX;
  ball->position.y = centerY;

  // center paddles
  userPaddle->centerPaddle();
  pcPaddle->centerPaddle();

  end = false;
  engine.uiCamera().y = -480;
}

void PongScene::render() {
  if (end) {
    // move the UI camera into position and change the state
    engine.uiCamera().y = 0;
    winText->setAnimationState(isUserWin ? "userWin" : "pcWin");
  }

  Wolf2D::Scene::render(); // render the scene as we should

  if (m_engine.deltaTime() < 0 || end)
    return; // don't do anything while deltaTime is undefined. thats the case
            // for the first frame or two.

  // AI (basic ass shit)
  doAIMove();

  // user input
  if (isMoving) {
    if (isMovingUp)
      userPaddle->nudgeUp();
    else
      userPaddle->nudgeDown();
  }

  // physics (not using SimplePhysics because... eh)
  doBallBounce();

  if (ball->position.x < 0) {
    // USER's wall, PC WIN
    end = true;
    isUserWin = false;
  }

  // imgui
#ifdef ENABLE_IMGUI
  ImGui::Begin("pong moment");
  ImGui::SliderFloat("move speed", &moveSpeed, 0, 128);
  ImGui::Text("ball pos: %fx%f", ball->position.x, ball->position.y);
  ImGui::Text("ball dir: %fx%f", dirX, dirY);
  if (ImGui::Button("reset ball pos")) {
    ball->position.x = 0;
    ball->position.y = 0;
  }

  ImGui::Text("FPS (rounded): %d\nDelta Time: %f",
              (int)round(1 / m_engine.deltaTime()), m_engine.deltaTime());

  ImGui::End();
#endif
}

void PongScene::doAIMove() {
  // get ball's center Y
  int ballY = ball->position.y + (ball->position.h / 2);
  // get AI paddle's center Y
  int paddleY = pcPaddle->position.y + (pcPaddle->position.h / 2);

  if (ballY > paddleY) // if ball is lower, nudge down
    pcPaddle->nudgeDown();
  else // otherwise, ball will be higher, so nudge up
    pcPaddle->nudgeUp();
}

void PongScene::doBallBounce() {
  ball->position.x += dirX * (moveSpeed * m_engine.deltaTime());
  ball->position.y += dirY * (moveSpeed * m_engine.deltaTime());

  // top-bottom collision
  if (ball->position.y + ball->position.h > m_engine.camera().h ||
      ball->position.y < 0) {
    dirY = -dirY;
  }

  // win collisions
  if (ball->position.x + ball->position.w > m_engine.camera().w) {
    // PC's wall, USER WIN
    end = isUserWin = true;
  }

  // paddle collisions
  if (
      // ball collides with user paddle
      (ball->position.x < userPaddle->position.x + userPaddle->position.w &&
       ball->position.x > userPaddle->position.x &&
       ball->position.y + ball->position.h >= userPaddle->position.y &&
       ball->position.y < userPaddle->position.y + userPaddle->position.h)
      // ball collides with PC paddle
      || (ball->position.x + ball->position.w > pcPaddle->position.x &&
          ball->position.x < pcPaddle->position.x + pcPaddle->position.w &&
          ball->position.y + ball->position.h >= pcPaddle->position.y &&
          ball->position.y < pcPaddle->position.y + pcPaddle->position.h)) {
    LOG_DEBUG("bounce!");
    dirX = -dirX;
    // force a move
    ball->position.x += dirX * (moveSpeed * m_engine.deltaTime());
    ball->position.y += dirY * (moveSpeed * m_engine.deltaTime());
    // kinda confusing, no likey
    // dirY = rand() % 2 ? -1 : 1;
  }
}
