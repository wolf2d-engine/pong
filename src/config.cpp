#include "engine/engine.h"
#include "engine/main.h"
#include "engine/rm_frontends/wolfpak.h"
#include "scene.h"
#include "wolfpak/wolfpak.h"

ENGINE_CONFIG {
  Wolf2D::Engine *engine = new Wolf2D::Engine(800, 600, "Wolf2D-Pong");

  engine->camera().w = 320;
  engine->camera().h = 240;

  engine->uiCamera().w = 640;
  engine->uiCamera().h = 480;

  // wolfpak :DDD
  WolfPak::WolfPak *pak = WolfPak::WolfPak::fromFile("data.wpak");
  if (pak == nullptr) {
    // no wpak, no game
    delete engine;
    return nullptr;
  }

  engine->resourceManager()->registerFrontend(
      new Wolf2D::RMFrontends::WolfPakFrontend(pak));

  // our scene
  PongScene *pongScene = new PongScene(*engine);
  engine->setScene(pongScene);

  return engine;
};
