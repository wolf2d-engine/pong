#include "paddle.h"
#include "engine/engine.h"
#include "engine/texture.h"
#include "globals.h"

Paddle::Paddle(Wolf2D::Engine &engine, int x) : Wolf2D::Texture(engine, *engine.resourceManager()->getResource("white.png"), x, 0) {
    position.w = 10;
    position.h = 56;
    
    centerPaddle();
};

void Paddle::nudgeDown() {
    position.y += moveSpeed * engine.deltaTime();
    if (position.y + position.h > engine.camera().h)
        position.y = engine.camera().h - position.h;
}

void Paddle::nudgeUp() {
    position.y -= moveSpeed * engine.deltaTime();
    if (position.y < 0) position.y = 0;
}

void Paddle::centerPaddle() {
    position.y = (engine.camera().h - position.h) / 2;
}
