#pragma once
#include "engine/texture.h"

class Paddle : public Wolf2D::Texture {
public:
    Paddle(Wolf2D::Engine &engine, int x);

    void nudgeDown();
    void nudgeUp();

    void centerPaddle();
};
