#pragma once
#include "engine/engine.h"
#include "engine/input/handler.h"
#include "engine/scene.h"
#include "engine/sprite.h"
#include "engine/ui/sprite.h"
#include "paddle.h"

class PongScene : public Wolf2D::Scene {
public:
  PongScene(Wolf2D::Engine &engine);

  void render();

private:
  void reset();
  void doAIMove();
  void doBallBounce();

  Wolf2D::Engine &m_engine;

  Wolf2D::Sprite *ball;
  Wolf2D::UI::Sprite *winText;

  Wolf2D::Input::Handler zHandler;

  Wolf2D::Input::Handler paddleUpHandler;
  Wolf2D::Input::Handler paddleDownHandler;

  Paddle *userPaddle;
  Paddle *pcPaddle;
};
